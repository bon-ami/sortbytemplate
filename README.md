# sortByTemplate

## Command line parameters

 - -t {template file}
 - -i {input csv file}
 - -o {output csv file}
 - -v for version info
 - -d for debugging
 
the only column in template file defines the order.
the first column in input csv will be matched, to sort by lines, where columns do not match are neglected. csv files are comma separated.