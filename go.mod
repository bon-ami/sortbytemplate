module gitlab.com/bon-ami/sortbytemplate

go 1.22.2

require gitee.com/bon-ami/eztools/v6 v6.2.0

require (
	github.com/Azure/go-ntlmssp v0.0.0-20221128193559-754e69321358 // indirect
	github.com/BurntSushi/toml v1.3.2 // indirect
	github.com/Xuanwo/go-locale v1.1.0 // indirect
	github.com/go-asn1-ber/asn1-ber v1.5.4 // indirect
	github.com/go-ldap/ldap/v3 v3.4.5 // indirect
	github.com/go-sql-driver/mysql v1.7.1 // indirect
	github.com/kr/binarydist v0.1.0 // indirect
	github.com/mongodb-forks/digest v1.0.5 // indirect
	github.com/nicksnyder/go-i18n/v2 v2.2.1 // indirect
	github.com/sanbornm/go-selfupdate v0.0.0-20230714125711-e1c03e3d6ac7 // indirect
	github.com/shibukawa/configdir v0.0.0-20170330084843-e180dbdc8da0 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/exp v0.0.0-20230905200255-921286631fa9 // indirect
	golang.org/x/net v0.15.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/term v0.12.0 // indirect
	golang.org/x/text v0.13.0 // indirect
)
