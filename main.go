package main

import (
	"flag"
	"os"

	"gitee.com/bon-ami/eztools/v6"
)

const (
	module = "sortbytemplate"
)

var (
	// Ver version
	Ver string
	// Bld build
	Bld string
)

func read(template, input string) ([][]string, error) {
	orderTemplate, err := eztools.CSVRead(template)
	if err != nil {
		eztools.ShowStrln("template read", err)
		return nil, err
	}
	if eztools.Debugging {
		eztools.ShowStrln("template read", orderTemplate)
	}
	orderIn, err := eztools.CSVRead(input)
	if err != nil {
		eztools.ShowStrln("input read", err)
		return nil, err
	}
	if eztools.Debugging {
		eztools.ShowStrln("input read", orderIn)
	}
	var orderOut [][]string
	for _, v := range orderTemplate {
		for _, v1 := range orderIn {
			if v[0] == v1[0] {
				orderOut = append(orderOut, v1)
			}
		}
	}
	if eztools.Debugging {
		eztools.ShowStrln("output to write", orderOut)
	}
	return orderOut, nil
}

func main() {
	var paramVer, paramDbg bool
	var paramTemplate, paramInput, paramOutput string
	flag.StringVar(&paramTemplate, "t", "", "template file with only column in required order")
	flag.StringVar(&paramInput, "i", "", "input csv, with no header, first column to match, other columns to go together")
	flag.StringVar(&paramOutput, "o", "", "output csv")
	flag.BoolVar(&paramVer, "v", false, "version info")
	flag.BoolVar(&paramDbg, "d", false, "debugging")
	flag.Parse()
	if paramVer {
		eztools.ShowStrln(module + " version " + Ver + " build " + Bld)
		return
	}
	if len(paramTemplate) < 1 || len(paramInput) < 1 || len(paramOutput) < 1 {
		eztools.ShowStrln("usage: sortbytemplate -t template.csv -i input.csv -o output.csv")
		eztools.ShowStrln("template.csv should contain one column only. lines withe first column not mached in input.csv are ignored. all csv files are comma separated.")
		os.Exit(1)
	}
	if paramDbg {
		eztools.Debugging = true
	}

	// self upgrade
	upch := make(chan bool, 2)
	go chkUpdate(upch)

	data, err := read(paramTemplate, paramInput)
	if err == nil && data != nil {
		err = eztools.CSVWrite(paramOutput, data)
	}

	for i := 0; i < 3; i++ {
		if !<-upch {
			break
		}
	}
	if err != nil {
		if eztools.Debugging {
			eztools.ShowStrln("exit with", err)
		}
		os.Exit(1)
	}
}

/*
upch <-      | false                               | true

	1st. | no check                            | to check
	2nd. | wrong update server config          |
	3rd. | wrong other config or check failure |
*/
func chkUpdate(upch chan bool) {
	if true { // disabled
		upch <- false
		return
	}
	var (
		db  *eztools.Db
		err error
	)
	db, _, err = eztools.MakeDb()
	if err != nil {
		if /*err == os.PathErr ||*/ err == eztools.ErrNoValidResults {
			eztools.ShowStrln("NO configuration for EZtools. Get one to auto update this app!")
		}
		upch <- false
		return
	}
	defer db.Close()
	upch <- true
	db.AppUpgrade(db.GetTblDef(), module, Ver, nil, upch)
}
